[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_individual4/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_individual4/-/commits/main)
# Coinflip Step Function W/ DynamoDB
The purpose of this project is to expirement with step functions. I do this by building three lambda functuons: one to flip a coin, one to insert to a db, and one to check how many heads have been flipped. 

## Youtube Video 
https://youtu.be/natrF_eVDIk


## State Machine w/ Succesful Example

![Screenshot_2024-04-17_at_9.06.22_AM](/uploads/2d98f0969c0e50e60342191c4abda682/Screenshot_2024-04-17_at_9.06.22_AM.png)

![Screenshot_2024-04-17_at_9.06.55_AM](/uploads/67ebaa3b6f6ce81db78bf6e478cbacab/Screenshot_2024-04-17_at_9.06.55_AM.png)

![Screenshot_2024-04-17_at_9.07.54_AM](/uploads/3f6e6d026e2aaff9c3b551ef5d6f7d90/Screenshot_2024-04-17_at_9.07.54_AM.png)


## Preparation 
1. Rust and Cargo Lambda are required for this project: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, and `brew install cargo-lambda`
2. An AWS account is required. Sign up for the free tier.
3. Set up your Cargo Lambda project `cargo lambda new new-lambda-project`
4. Modify the template with your own additions or deletions.
5. Do `cargo lambda watch` to test locally.
6. Do `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test the Lambda function (arguments passed in may be different).
7. Go to the AWS IAM web management console and add an IAM User for credentials.
8. Attach policies `lambdafullaccess` and `iamfullaccess`.
9. Finish user creation, open user details, and go to security credentials.
10. Generate access key.
11. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in a `.env` file that won't be pushed with your repo (add to .gitignore).
12. Export the vars by using `export` in the terminal. This lets cargo lambda know which AWS account to deploy to.
13. Build your project for release `cargo lambda build --bin <name of bin> --release`.
14. Deploy your project for release `cargo lambda deploy <name of bin>`.
15. You do step 13 and 14 for the number of lambda functuions you want in your step function 
16. Confirm all of your lambda fucntions can work indepedently (if you have a DynamoDB interaction make sure your lmabda function has the role to access DynamoDB)
17. Make sure your first lambda fucntion response matches the request input of the second lambda function and so on 
18. Once you confirm that, go to "State Machines" in AWS 
19. Build your state machine 
20. Test your state machine the result wil be the response of your final step function

## References
1. https://www.youtube.com/watch?v=qoVAEflgcZ4
2. https://www.cargo-lambda.info/guide/getting-started.html
