use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

// data to push to the dynamodb table
#[derive(Debug, Serialize, Deserialize)]
pub struct CoinFlip {
    pub flip_id: String,
    pub flip_value: String,
}
// required for AWS
#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

// implement Display for the Failure response so that we can then implement Error.
impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    req_id: String,
    msg: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let req_id = event.payload.req_id;
    let msg = event.payload.msg;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let mut coin_flip = CoinFlip {
        flip_id: String::new(),
        flip_value: String::new(),
    };
    // set value
    coin_flip.flip_id = String::from(req_id.clone());
    coin_flip.flip_value = String::from(msg);

    let coin_flip_id = AttributeValue::S(coin_flip.flip_id.clone());
    let coin_flip_value = AttributeValue::S(coin_flip.flip_value.to_string());
    // add to dynamodb
    // store our data in the coinflip table
    let _resp = client
        .put_item()
        .table_name("coinflip")
        .item("flip_id", coin_flip_id)
        .item("flip_value", coin_flip_value)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        });

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: "Inserted into db".to_string(),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
